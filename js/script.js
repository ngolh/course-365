/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCOURSES_DB = [];
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function(){
    callApiGetCourseDB();
    onPageLoading();
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm onpageLoad
function onPageLoading(){
    console.log("đã load");
    displayRecommendCourse(gCOURSES_DB);
    displayPopularCourse(gCOURSES_DB);
    displayTrendingCourse(gCOURSES_DB);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm gọi API laays danh sách khóa học
function callApiGetCourseDB(){
    $.ajax({
        type: "GET",
        url: gBASE_URL + "/courses",
        dataType: "json",
        async: false,
        success: function (response) {
            gCOURSES_DB = response;
        }
    });
}

// hàm hiển thị khóa học recomend
function displayRecommendCourse(paramCourseData){
    $("#recommended-course").html("");
    for(var bI = 0 ; bI < 4 ; bI++){
        if(paramCourseData[bI].isPopular == true || paramCourseData[bI].isTrending == true){
            $("#recommended-course").append(`
            <div class="col-lg-3 col-md-6 col-sm-12 courses">
                <div class="card-courses">
                    <img src="${paramCourseData[bI].coverImage}" width="100%" class="thump-img">
                    <div class="main-card-courses">
                        <div class="course-name">
                        ${paramCourseData[bI].courseName}
                        </div>
                        <div class="course-lv d-flex align-items-center">
                            <i class="far fa-clock"></i>
                            <div class="course-time">
                                ${paramCourseData[bI].duration}
                            </div>
                            <div class="lv">
                                ${paramCourseData[bI].level}
                            </div>
                        </div>
                        <div class="course-price d-flex">
                            <div class="new-price">
                                $${paramCourseData[bI].discountPrice}
                            </div>
                            <div class="old-price">
                                $${paramCourseData[bI].price}
                            </div>
                        </div>
                    </div>
                    <div class="teacher-course d-flex align-items-center">
                        <div class="teacher-details d-flex align-items-center">
                            <img src="${paramCourseData[bI].teacherPhoto}" alt="morris_mccoy" class="rounded-circle" width="18%">
                            <div class="teacher-name">
                                ${paramCourseData[bI].teacherName}
                            </div>
                        </div>
                        <div class="mark-icon">
                            <i class="far fa-bookmark"></i>
                        </div>
                    </div>
                </div>
            </div>
            `);
        }
    }
}
// hàm hiển thị khóa học popular
function displayPopularCourse(paramCourseData){
    $("#popular-course").html("");
    var vCountCourse = 0;
    for(var bI = 0 ; bI < paramCourseData.length ; bI++){
        if(paramCourseData[bI].isPopular == true){
            vCountCourse++;
            if(vCountCourse < 5){
                $("#popular-course").append(`
                <div class="col-lg-3 col-md-6 col-sm-12 courses">
                    <div class="card-courses">
                        <img src="${paramCourseData[bI].coverImage}" width="100%" class="thump-img">
                        <div class="main-card-courses">
                            <div class="course-name">
                            ${paramCourseData[bI].courseName}
                            </div>
                            <div class="course-lv d-flex align-items-center">
                                <i class="far fa-clock"></i>
                                <div class="course-time">
                                    ${paramCourseData[bI].duration}
                                </div>
                                <div class="lv">
                                    ${paramCourseData[bI].level}
                                </div>
                            </div>
                            <div class="course-price d-flex">
                                <div class="new-price">
                                    $${paramCourseData[bI].discountPrice}
                                </div>
                                <div class="old-price">
                                    $${paramCourseData[bI].price}
                                </div>
                            </div>
                        </div>
                        <div class="teacher-course d-flex align-items-center">
                            <div class="teacher-details d-flex align-items-center">
                                <img src="${paramCourseData[bI].teacherPhoto}" alt="morris_mccoy" class="rounded-circle" width="18%">
                                <div class="teacher-name">
                                    ${paramCourseData[bI].teacherName}
                                </div>
                            </div>
                            <div class="mark-icon">
                                <i class="far fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
                </div>
                `);
            }
        }
    }
}
// hàm hiển thị khóa học trending
function displayTrendingCourse(paramCourseData){
    $("#trending-course").html("");
    var vCountCourse = 0;
    for(var bI = 0 ; bI < paramCourseData.length ; bI++){
        if(paramCourseData[bI].isTrending == true){
            vCountCourse++;
            if(vCountCourse < 5){
                $("#trending-course").append(`
                <div class="col-lg-3 col-md-6 col-sm-12 courses">
                    <div class="card-courses">
                        <img src="${paramCourseData[bI].coverImage}" width="100%" class="thump-img">
                        <div class="main-card-courses">
                            <div class="course-name">
                            ${paramCourseData[bI].courseName}
                            </div>
                            <div class="course-lv d-flex align-items-center">
                                <i class="far fa-clock"></i>
                                <div class="course-time">
                                    ${paramCourseData[bI].duration}
                                </div>
                                <div class="lv">
                                    ${paramCourseData[bI].level}
                                </div>
                            </div>
                            <div class="course-price d-flex">
                                <div class="new-price">
                                    $${paramCourseData[bI].discountPrice}
                                </div>
                                <div class="old-price">
                                    $${paramCourseData[bI].price}
                                </div>
                            </div>
                        </div>
                        <div class="teacher-course d-flex align-items-center">
                            <div class="teacher-details d-flex align-items-center">
                                <img src="${paramCourseData[bI].teacherPhoto}" alt="morris_mccoy" class="rounded-circle" width="18%">
                                <div class="teacher-name">
                                    ${paramCourseData[bI].teacherName}
                                </div>
                            </div>
                            <div class="mark-icon">
                                <i class="far fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
                </div>
                `);
            }
        }
    }
}